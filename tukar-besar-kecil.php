<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
function tukar_besar_kecil($string){
//kode di sini
$kecil = "abcdefghijklmnopqrstuvwxyz";
$output = "";
for($a = 0; $a < strlen($string); $a++){
    $apakecil = strpos($kecil, $string[$a]); 
    if($apakecil != null){
        $output = $output . strtoupper($string[$a]);
    }else {
        $output = $output . strtolower($string[$a]);
    }
}
return $output . "<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>
</body>
</html>