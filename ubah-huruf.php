<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
function ubah_huruf($string){
//kode di sini
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $output = "";
    for ($a = 0; $a < strlen($string); $a++){
        $position = strpos($abjad, $string[$a]);
        $output = $output . substr($abjad, $position + 1, 1);
    }
    return $output . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
</body>
</html>